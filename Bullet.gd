extends Area2D


export var speed = 600


func _ready():
	$AudioFire.play()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var velocity = Vector2(0, -1)
	velocity = velocity.normalized() * speed
	position += velocity * delta
