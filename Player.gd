extends Area2D

export var speed := 200

var viewport_size : Vector2
var sprite_offset : float
var bullet_scene = load("res://Bullet.tscn")
var root_node : Node
var can_shoot: bool = true



func _ready():
	viewport_size = get_viewport_rect().size
	sprite_offset = $Sprite.get_rect().size.x / 2
	root_node = get_tree().get_root()

	$ReloadTimer.connect("timeout", self, "_on_reload_timer_timeout")


func _process(delta):
	shoot()
	var velocity := get_normalized_velocity()
	set_position_in_screen(velocity, delta)


func shoot():
	if can_shoot and Input.is_action_just_pressed("ui_select"):
		can_shoot = false
		var bullet = bullet_scene.instance()
		bullet.set_name("bullet!")
		bullet.position = position + Vector2(0, -10)
		root_node.add_child(bullet)


func get_normalized_velocity() -> Vector2:
	"""Based on inputs, calculate next frame's velocity."""
	var velocity := Vector2()
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	velocity = velocity.normalized() * speed
	return velocity


func set_position_in_screen(velocity, delta):
	"""Set new position, ensure new position within bounds of screen."""
	var min_x_position = 0 + sprite_offset
	var max_x_position = viewport_size.x - sprite_offset

	position += velocity * delta
	position.x = clamp(
		position.x,
		min_x_position,
		max_x_position
	)


func _on_reload_timer_timeout():
	can_shoot = true
